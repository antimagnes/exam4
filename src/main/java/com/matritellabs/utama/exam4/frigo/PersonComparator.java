package com.matritellabs.utama.exam4.frigo;

import java.util.Comparator;

public class PersonComparator implements Comparator {

    @Override
    public int compare(Object o1, Object o2) {
        if (o1 == null || o2 == null) {
            throw new NullPointerException("some parameter was null");
        }
        Person person1 = (Person) o1;
        Person person2 = (Person) o2;
        if (person1.getBirthday().compareTo(person2.getBirthday()) == 0) {
            if (person1.getName().compareTo(person2.getName()) == 0) {
                if (person1.getMothersName().compareTo(person2.getMothersName()) == 0) {
                    return 0;
                } else {
                    return person1.getMothersName().compareTo(person2.getMothersName());
                }
            } else {
                return person1.getName().compareTo(person2.getName());
            }
        } else {
            return person1.getBirthday().compareTo(person2.getBirthday());
        }
    }
}
