package com.matritellabs.utama.exam4.frigo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PersonListLoader {

    private static Logger log = LoggerFactory.getLogger(PersonListLoader.class);
    private static SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd");


    public static List<Person> readPersonsFromFile(String fileURL, PersonFileFormat fileFormat) throws
            FileNotFoundException, IOException, RuntimeException {
        File theFile;
        List<Person> result = null;
        try {
            log.info("Loading file {}", fileURL);
            theFile = new File(new URI(fileURL));
            List<String> linesInThFile = Files.readAllLines(theFile.toPath());
            log.debug("Read content from file {}", linesInThFile);


            switch (fileFormat) {
                case CSV:
                    result = readCSVFromFileContent(linesInThFile);
                    break;
                case ONE_DATA_PER_LINE:
                    result = readODPLFromFileContent(linesInThFile);
                    break;
            }
        } catch (IllegalArgumentException | URISyntaxException e) {
            throw new FileNotFoundException("Something happend while loading file " + fileURL);
        }
        return result;
    }

    private static List<Person> readODPLFromFileContent(List<String> linesInThFile) throws RuntimeException {
        List<Person> result = new ArrayList<>();
        if (linesInThFile.size() % 4 != 0) {
            String message = "Lines in file not divisible by 4";
            log.error(message);
            throw new RuntimeException(message);
        }
        for (int i = 0; i < linesInThFile.size(); i = i + 4) {
            String[] parts = new String[4];
            parts[0] = linesInThFile.get(i);
            parts[1] = linesInThFile.get(i + 1);
            parts[2] = linesInThFile.get(i + 2);
            parts[3] = linesInThFile.get(i + 3);
            log.debug("Trying to process ODPL line {}, {}, {}, {}", parts[0], parts[1], parts[2], parts[3]);

            try {
                Person e = new Person(parts[0], SDF.parse(parts[1]), parts[2], parts[3]);
                result.add(e);
                log.info("Result is {}", e);
            } catch (ParseException e) {
                String message = "date " + parts[1] + " has an illegal format";
                log.error(message);
                throw new RuntimeException(message);
            }
        }
        return result;
    }

    private static List<Person> readCSVFromFileContent(List<String> fileLines) throws RuntimeException {
        List<Person> result = new ArrayList<>();
        for (String currentLine : fileLines) {
            log.debug("Trying to process CSV line {}", currentLine);
            String[] parts = currentLine.split(";");
            if (parts.length != 4) {
                String message = "File format is illegal, line hasn't got 3 semicolons " + currentLine;
                log.error(message);
                throw new RuntimeException(message);
            }
            try {
                Person e = new Person(parts[0], SDF.parse(parts[1]), parts[2], parts[3]);
                result.add(e);
                log.info("Result is {}", e);
            } catch (ParseException e) {
                String message = "date " + parts[1] + " has an illegal format in line " + currentLine;
                log.error(message);
                throw new RuntimeException(message);
            }
        }
        return result;
    }

    public static void writePersonsToFile(String fileURL, PersonFileFormat fileFormat, List<Person> personList) throws
            FileNotFoundException, IOException, RuntimeException {

        List<Person> copy = new ArrayList<>(personList);
        Collections.sort(copy, new PersonComparator());
        try {
            log.info("Loading file {}", fileURL);
            File theFile = new File(new URI(fileURL));
            List<String> result = null;
            log.debug("Writing {} content to file {}", personList, fileURL);

            switch (fileFormat) {
                case CSV:
                    result = generateCSVFileLines(personList);
                    break;
                case ONE_DATA_PER_LINE:
                    result = generateODPLFileLines(personList);
                    break;
            }
            BufferedWriter writer = new BufferedWriter(new FileWriter(theFile));

             // TODO make it work for empty lists

            for (int i = 0; i < result.size(); i++) {
                String line = result.get(i);
                writer.write(line);
                if (i != result.size() - 1) {
                    writer.newLine();
                }
            }
            writer.close();
        } catch (IllegalArgumentException | URISyntaxException e) {
            throw new FileNotFoundException("Something happend while loading file " + fileURL);
        }
    }

    private static List<String> generateCSVFileLines(List<Person> personList) {
        List<String> result = new ArrayList<>();
        for (Person p : personList) {
            result.add(String.join(";", p.getName(), SDF.format(p.getBirthday()), p.getMothersName(), p.getAddress()));
        }
        return result;
    }

    private static List<String> generateODPLFileLines(List<Person> personList) {
        List<String> result = new ArrayList<>();
        for (Person p : personList) {
            result.add(p.getName());
            result.add(SDF.format(p.getBirthday()));
            result.add(p.getMothersName());
            result.add(p.getAddress());
        }
        return result;
    }

    public static void main(String[] args) throws IOException {
        try {
//            System.out.println(PersonListLoader.readPersonsFromFile(
//                    "file:///home/frigo/Development/matritel/java_training/utama/exam4/src/test/resources" +
//                            "/empty_person" +
//                            ".csv",
//                    PersonFileFormat.CSV));
//            System.out.println(PersonListLoader.readPersonsFromFile(
//                    "file:///home/frigo/Development/matritel/java_training/utama/exam4/src/test/resources" +
//                            "/single_person" +
//                            ".csv",
//                    PersonFileFormat.CSV));
//            System.out.println(PersonListLoader.readPersonsFromFile(
//                    "file:///home/frigo/Development/matritel/java_training/utama/exam4/src/test/resources" +
//                            "/multiple_person" +
//                            ".csv",
//                    PersonFileFormat.CSV));
//            System.out.println(PersonListLoader.readPersonsFromFile(
//                    "file:///home/frigo/Development/matritel/java_training/utama/exam4/src/test/resources" +
//                            "/empty_person" +
//                            ".odpl",
//                    PersonFileFormat.ONE_DATA_PER_LINE));
//            System.out.println(PersonListLoader.readPersonsFromFile(
//                    "file:///home/frigo/Development/matritel/java_training/utama/exam4/src/test/resources" +
//                            "/single_person" +
//                            ".odpl",
//                    PersonFileFormat.ONE_DATA_PER_LINE));
//            System.out.println(PersonListLoader.readPersonsFromFile(
//                    "file:///home/frigo/Development/matritel/java_training/utama/exam4/src/test/resources" +
//                            "/multiple_person" +
//                            ".odpl",
//                    PersonFileFormat.ONE_DATA_PER_LINE));


            PersonListLoader.writePersonsToFile(
                    "file:///home/frigo/Development/matritel/java_training/utama/exam4/src/test/resources" +
                            "/multiple_person.odpl.written", PersonFileFormat.ONE_DATA_PER_LINE,
                    PersonListLoader.readPersonsFromFile(
                    "file:///home/frigo/Development/matritel/java_training/utama/exam4/src/test/resources" +
                            "/multiple_person.odpl", PersonFileFormat.ONE_DATA_PER_LINE));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
