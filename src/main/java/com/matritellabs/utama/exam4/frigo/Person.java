package com.matritellabs.utama.exam4.frigo;

import java.util.Date;

public class Person {
    private String name;
    private Date birthday;
    private String mothersName;
    private String address;

    public Person(String name, Date birthday, String mothersName, String address) {
        this.name = name;
        this.birthday = birthday;
        this.mothersName = mothersName;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public Date getBirthday() {
        return birthday;
    }

    public String getMothersName() {
        return mothersName;
    }

    public String getAddress() {
        return address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Person person = (Person) o;

        if (!name.equals(person.name)) return false;
        if (!birthday.equals(person.birthday)) return false;
        return mothersName.equals(person.mothersName);
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + birthday.hashCode();
        result = 31 * result + mothersName.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", birthday=" + birthday +
                ", mothersName='" + mothersName + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
