package com.matritellabs.utama.exam4.frigo;

public enum PersonFileFormat {
    CSV,
    ONE_DATA_PER_LINE
}
