import com.matritellabs.utama.exam4.frigo.Person;
import org.junit.Test;
import static org.junit.Assert.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PersonTest {

    private static SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd");

    @Test
    public void Person_equals_returns_true_for_equal_persons() {
        try {
            Person person1 = new Person("Han Solo", SDF.parse("1942-07-13"), "Jaina", "Millenium Falcon");
            Person person2 = new Person("Han Solo", SDF.parse("1942-07-13"), "Jaina", "Death star");
            assertEquals(person1, person2);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void Person_equals_returns_false_for_unequal_persons() {
        try {
            Person person1 = new Person("Han Solo", SDF.parse("1942-07-13"), "Jaina", "Millenium Falcon");
            Person person2 = new Person("Han Solo Yoda", SDF.parse("1942-07-13"), "Jaina", "Death star");
            assertNotEquals(person1, person2);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void Person_hashcode_returns_same_number_for_same_options() {
        try {
            Person person1 = new Person("Han Solo", SDF.parse("1942-07-13"), "Jaina", "Millenium Falcon");
            Person person2 = new Person("Han Solo", SDF.parse("1942-07-13"), "Jaina", "Death star");
            assertEquals(person1.hashCode(), person2.hashCode());
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
